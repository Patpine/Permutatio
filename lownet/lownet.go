package lownet

import (
	"Permutatio/binformat"
	"bytes"
	"fmt"
	"io"
	"net"
	"os"
	"syscall"
)

type Listener struct {
	l *net.UnixListener
}

type Conn struct {
	c *net.UnixConn
}

func Listen(fpath string) (*Listener, error) {
	err := syscall.Unlink(fpath)
	if err != nil && !os.IsNotExist(err) {
		return nil, fmt.Errorf("failed to remove previous listener while opening new one: %w", err)
	}

	addr, err := net.ResolveUnixAddr("unix", fpath)
	if err != nil {
		return nil, fmt.Errorf("failed to resolve unix address while opening listener: %w", err)
	}

	ul, err := net.ListenUnix("unix", addr)
	if err != nil {
		return nil, fmt.Errorf("failed to create unixlistener while opening listener: %w", err)
	}

	err = os.Chmod(fpath, 0777)
	if err != nil {
		return nil, fmt.Errorf("failed to make listener open to all: %w", err)
	}

	return &Listener{l: ul}, nil
}

func Dial(fpath string) (*Conn, error) {
	addr, err := net.ResolveUnixAddr("unix", fpath)
	if err != nil {
		return nil, fmt.Errorf("failed to resolve unix address while dialing connection: %w", err)
	}
	uc, err := net.DialUnix("unix", nil, addr)
	if err != nil {
		return nil, fmt.Errorf("failed to create unixconnection while dialing connection: %w", err)
	}
	return &Conn{c: uc}, nil
}

func (l *Listener) Accept() (*Conn, error) {
	uc, err := l.l.AcceptUnix()
	if err != nil {
		return nil, err
	}
	return &Conn{c: uc}, nil
}

type lownetDeserCtx struct {
	io.Reader
	fds []int
}

// RecvFileHandle implements binformat.DeserializationContext
func (l lownetDeserCtx) RecvFileHandle(index uint64) (hdl uintptr, ok bool) {
	if int(index) >= len(l.fds) {
		return 0, false
	}
	return uintptr(l.fds[index]), true
}

var _ binformat.DeserializationContext = lownetDeserCtx{}

func (c *Conn) Receive() (binformat.Msg, error) {
	var msg [2048]byte
	var oob [2048]byte

	msgn, oobn, _, _, err := c.c.ReadMsgUnix(msg[:], oob[:])
	if err != nil {
		return nil, fmt.Errorf("error reading unix msg: %w", err)
	}

	cmsgs, err := syscall.ParseSocketControlMessage(oob[0:oobn])
	if err != nil {
		return nil, fmt.Errorf("issue parsing socket control message: %w", err)
	} else if len(cmsgs) != 1 {
		return nil, fmt.Errorf("invalid number of control messages received")
	}

	fds, err := syscall.ParseUnixRights(&cmsgs[0])
	if err != nil {
		return nil, fmt.Errorf("issue parsing unix rights: %w", err)
	}

	ctx := lownetDeserCtx{bytes.NewReader(msg[0:msgn]), fds}
	return binformat.Deserialize(ctx)
}

type lownetSerCtx struct {
	bytes.Buffer
	fds []uintptr
}

func (ctx *lownetSerCtx) SendFileHandle(hdl uintptr) (index uint64, ok bool) {
	ctx.fds = append(ctx.fds, hdl)
	return uint64(len(ctx.fds) - 1), true
}

var _ binformat.SerializationContext = &lownetSerCtx{}

func (c *Conn) Send(b binformat.Msg) error {
	ctx := lownetSerCtx{}
	err := binformat.Serialize(b, &ctx)
	if err != nil {
		return fmt.Errorf("failed to serialize message whilst sending message: %w", err)
	}
	msg := ctx.Bytes()
	rights := syscall.UnixRights(0)
	_, _, err = c.c.WriteMsgUnix(msg, rights, nil)
	if err != nil {
		return fmt.Errorf("failed to write unix message: %w", err)
	}
	return nil
}
