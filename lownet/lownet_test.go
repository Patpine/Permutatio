package lownet_test

import (
	"Permutatio/binformat"
	"Permutatio/lownet"
	"os"
	"testing"
)

func TestFDTransfer(t *testing.T) {
	sock, err := lownet.Listen("orwell.sock")
	if err != nil {
		t.Fatal(err)
	}
	errs := make(chan error)
	go func() {
		f, err := lownet.Dial("orwell.sock")
		if err != nil {
			errs <- err
			return
		}
		woot, err := os.OpenFile("/dev/null", os.O_WRONLY, os.ModeAppend)
		if err != nil {
			errs <- err
			return
		}
		err = f.Send(binformat.Handle{Descriptor: woot.Fd()})
		if err != nil {
			errs <- err
			return
		}
		close(errs)
	}()
	v, ok := <-errs
	if ok {
		t.Fatal(v)
	}
	conn, err := sock.Accept()
	if err != nil {
		t.Fatal(err)
	}
	msg, err := conn.Receive()
	if err != nil {
		t.Fatal(err)
	}
	h, ok := msg.(binformat.Handle)
	if !ok {
		t.Fatal(err)
	}
	fi := os.NewFile(h.Descriptor, "/dev/null")
	if fi == nil {
		t.Fatalf("failed to receive file descriptor")
	}
}
