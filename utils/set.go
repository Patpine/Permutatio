package utils

type Set[C comparable] struct {
	m map[C]struct{}
}

func (c Set[C]) Contains(val C) bool {
	_, ok := c.m[val]
	return ok
}

func (c Set[C]) Add(val C) {
	c.m[val] = struct{}{}
}

func NewSet[C comparable]() Set[C] {
	return Set[C]{map[C]struct{}{}}
}
