package utils

type Option[C any] interface {
	isOption()
	Value() C
	HasValue() bool
}

func Some[C any](val C) Option[C] {
	return some[C]{Val: val}
}

func None[C any]() none[C] {
	return none[C]{}
}

type some[C any] struct {
	Val C
}

func (s some[C]) isOption() {}

func (s some[C]) HasValue() bool {
	return true
}

func (s some[C]) Value() C {
	return s.Val
}

type none[C any] struct {
}

func (n none[C]) isOption() {}

var _ Option[interface{}] = some[interface{}]{}
var _ Option[interface{}] = none[interface{}]{}

func (n none[C]) HasValue() bool {
	return false
}

func (n none[C]) Value() C {
	panic("Attempted to unwrap a none")
}
