package utils

type Map[Key comparable, Val any] struct {
	m map[Key]Val
}

func NewMap[Key comparable, Val any]() Map[Key, Val] {
	return Map[Key, Val]{map[Key]Val{}}
}

func (m *Map[Key, Val]) Contains(k Key) bool {
	_, ok := m.m[k]
	return ok
}

func (m *Map[Key, Val]) Add(k Key, v Val) {
	m.m[k] = v
}

func (m *Map[Key, Val]) Get(k Key) Option[Val] {
	if v, ok := m.m[k]; ok {
		return Some(v)
	}
	return None[Val]()
}
