package main

import (
	"Permutatio/checking"
	"Permutatio/cst"
	"os"
	"path"
)

func main() {
	// obj := Object{
	// 	Fields: map[uint64]Msg{
	// 		1: Integer{Value: 10},
	// 		2: Float{Value: 3.5},
	// 		3: Data{Value: []byte("youre moms r gay <3")},
	// 		4: Object{
	// 			Fields: map[uint64]Msg{
	// 				1: Integer{Value: 10},
	// 				2: Float{Value: 3.5},
	// 				3: Data{Value: []byte("youre moms r gay <3")},
	// 				5: Object{
	// 					Fields: map[uint64]Msg{
	// 						1: Integer{Value: 10},
	// 						2: Float{Value: 3.5},
	// 						3: Data{Value: []byte("youre moms r gay <3")},
	// 					},
	// 				},
	// 				7: &Array[Integer]{
	// 					Elems: []Integer{
	// 						{1},
	// 						{3},
	// 						{5},
	// 						{8},
	// 					},
	// 				},
	// 			},
	// 		},
	// 	},
	// }
	// dc := &DummySerializationContext{}
	// err := Serialize(obj, dc)
	// if err != nil {
	// 	panic(err)
	// }
	// os.Stdout.Write(dc.Bytes())

	fdata, err := os.ReadFile(os.Args[1])
	if err != nil {
		panic(err)
	}
	file, err := cst.Parser.ParseBytes(os.Args[1], fdata)
	if err != nil {
		panic(err)
	}

	e := checking.NewEnvironment()
	mod, err := e.Check(*file, path.Dir(os.Args[1]))
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}
	_ = mod
	// repr.Println(mod)
}
