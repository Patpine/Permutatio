package module

func TypeEquals(lhs, rhs Type) bool {
	if lhs.Module() != rhs.Module() || lhs.Name() != rhs.Name() {
		return false
	}
	switch v := lhs.(type) {
	case *AtomicType:
		return true
	case *ProductType:
		return true
	case *SumType:
		return true
	case *ArrayType:
		return TypeEquals(v.Element, rhs.(*ArrayType).Element)
	default:
		panic("Unreachable")
	}
}
