package module

var BuiltinModule Module

var Int = &AtomicType{Parent: &BuiltinModule, Identifier: "Int"}
var UInt = &AtomicType{Parent: &BuiltinModule, Identifier: "UInt"}
var Float = &AtomicType{Parent: &BuiltinModule, Identifier: "Float"}
var String = &AtomicType{Parent: &BuiltinModule, Identifier: "String"}
var Binary = &AtomicType{Parent: &BuiltinModule, Identifier: "Binary"}
var Handle = &AtomicType{Parent: &BuiltinModule, Identifier: "Handle"}

func init() {
	BuiltinModule.Types = []Type{Int, UInt, Float, String, Binary, Handle}
}
