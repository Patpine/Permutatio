package module

import "Permutatio/utils"

type Module struct {
	// name
	Identifier string

	// structure
	Types         []Type
	RPCs          []*RPC
	Notifications []*Notification
}

func (m *Module) Module() *Module {
	return nil
}

func (m *Module) Name() string {
	return m.Identifier
}

func (m *Module) Child(name string) utils.Option[Object] {
	for _, t := range m.Types {
		if t.Name() == name {
			return utils.Some(t.(Object))
		}
	}
	return utils.None[Object]()
}

type Object interface {
	Module() *Module
	Name() string
	Child(n string) utils.Option[Object]
}

type Type interface {
	Object
	Errorable() bool

	isType()
}

type ErrorType struct {
	Parent     *Module
	Identifier string
}

func (ErrorType) Child(n string) utils.Option[Object] {
	return utils.None[Object]()
}

func (e ErrorType) Module() *Module {
	return e.Parent
}

func (e ErrorType) Name() string {
	return e.Identifier
}

func (e ErrorType) Errorable() bool {
	return true
}

func (e *ErrorType) isType() {

}

type Field struct {
	Name   string
	Type   Type
	Number uint
}

type SumType struct {
	// name
	Parent     *Module
	Identifier string
	Error      bool

	// structure
	Constructors []struct {
		Name   string
		Number uint
		Fields []Field
	}
}

func (SumType) Child(n string) utils.Option[Object] {
	return utils.None[Object]()
}
func (s SumType) Errorable() bool {
	return s.Error
}
func (s SumType) Module() *Module {
	return s.Parent
}
func (s SumType) Name() string {
	return s.Identifier
}
func (*SumType) isType() {}

type ProductType struct {
	// name
	Parent     *Module
	Identifier string
	Error      bool

	// structure
	Fields []Field
}

func (ProductType) Child(n string) utils.Option[Object] {
	return utils.None[Object]()
}
func (p ProductType) Errorable() bool {
	return p.Error
}
func (p ProductType) Module() *Module {
	return p.Parent
}
func (p ProductType) Name() string {
	return p.Identifier
}
func (*ProductType) isType() {}

type AtomicType struct {
	// name
	Parent     *Module
	Identifier string
}

func (AtomicType) Child(n string) utils.Option[Object] {
	return utils.None[Object]()
}
func (a AtomicType) Errorable() bool {
	return false
}
func (a AtomicType) Module() *Module {
	return a.Parent
}
func (a AtomicType) Name() string {
	return a.Identifier
}
func (*AtomicType) isType() {}

type ArrayType struct {
	Element Type
}

func (ArrayType) Child(n string) utils.Option[Object] {
	return utils.None[Object]()
}
func (a ArrayType) Errorable() bool {
	return false
}

func (a ArrayType) Module() *Module {
	return &BuiltinModule
}

func (a ArrayType) Name() string {
	return "Array"
}

func (*ArrayType) isType() {}

type RPC struct {
	Parent     *Module
	Identifier string

	Arguments   []Field
	ReturnValue utils.Option[Type]
	// ThrowValue's Type must be a ProductType or a SumType marked as an error type
	ThrowValue utils.Option[Type]
}

func (*RPC) Child(n string) utils.Option[Object] {
	return utils.None[Object]()
}
func (r *RPC) Module() *Module {
	return r.Parent
}
func (r *RPC) Name() string {
	return r.Identifier
}

var _ Object = &RPC{}

type Notification struct {
	Parent     *Module
	Identifier string

	Arguments []Field
}

func (*Notification) Child(n string) utils.Option[Object] {
	return utils.None[Object]()
}
func (n *Notification) Module() *Module {
	return n.Parent
}
func (n *Notification) Name() string {
	return n.Identifier
}

var _ Object = &RPC{}
