package cst

import (
	"github.com/alecthomas/participle/v2"
	"github.com/alecthomas/participle/v2/lexer"
)

type File struct {
	Package      string        `"package" @Ident ";"`
	Declarations []Declaration `(@@ ";")*`
}

type Field struct {
	Name        string `@Ident ":"`
	Type        Type   `@@`
	FieldNumber int    `("@" | "at") @Int`
}

type Declaration interface{ isDecl() }

type Type interface{ isType() }

type ArrayType struct {
	Element Type `"[" @@ "]"`
	Pos     lexer.Position
}

func (ArrayType) isType() {}

type TypeReference struct {
	Names []string `@Ident ("/" @Ident)*`
	Pos   lexer.Position
}

func (TypeReference) isType() {}

type SumType struct {
	Errorable    bool   `@"error"?`
	Name         string `"enum" @Ident "="`
	Constructors []struct {
		Name        string  `"|" @Ident`
		FieldNumber int     `("@" | "at") @Int`
		Fields      []Field `( "(" @@? ("," @@)* ")")?`
		Pos         lexer.Position
	} `@@*`
	Pos lexer.Position
}

func (SumType) isDecl() {}

type ProductType struct {
	Errorable bool    `@"error"?`
	Name      string  `"struct" @Ident "{"`
	Fields    []Field `(@@ ";")* "}"`
	Pos       lexer.Position
}

func (ProductType) isDecl() {}

type RPC struct {
	Name      string  `"rpc" @Ident "("`
	Arguments []Field `@@? ("," @@)* ")"`
	Returns   *Type   `("returns" @@)?`
	Throws    *Type   `("throws" @@)?`
	Pos       lexer.Position
}

func (RPC) isDecl() {}

type Notification struct {
	Name      string  `"notif" @Ident "("`
	Arguments []Field `@@? ("," @@)* ")"`
	Pos       lexer.Position
}

func (Notification) isDecl() {}

type Import struct {
	Path     []string `"import" @Ident ("/" @Ident)*`
	Exposing []struct {
		Name string `@Ident`
		Pos  lexer.Position
	} `("(" @@ ("," @@)* ")")?`
	Pos lexer.Position
}

func (Import) isDecl() {}

var Parser = participle.MustBuild[File](
	participle.Union[Declaration](
		SumType{},
		ProductType{},
		RPC{},
		Notification{},
		Import{},
	),
	participle.Union[Type](
		ArrayType{},
		TypeReference{},
	),
)
