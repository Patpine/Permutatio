package binformat

import (
	"bytes"
	"encoding/binary"
	"fmt"
)

type Msg interface {
	isMsg()
	serialize(to SerializationContext) error
	Tag() byte
	Equals(other Msg) bool
}

func Serialize(msg Msg, to SerializationContext) error {
	err := binary.Write(to, binary.LittleEndian, msg.Tag())
	if err != nil {
		return err
	}
	err = msg.serialize(to)
	if err != nil {
		return err
	}
	return nil
}

type Object struct {
	Fields map[uint64]Msg
}

func (Object) isMsg() {}

var _ Msg = Object{}

func (o Object) Tag() byte {
	return 0
}

func (o Object) Equals(other Msg) bool {
	v, ok := other.(Object)
	if !ok {
		return false
	}
	if len(v.Fields) != len(o.Fields) {
		return false
	}
	for k := range o.Fields {
		_, ok := v.Fields[k]
		if !ok {
			return false
		}
	}
	return true
}

func (o Object) serialize(to SerializationContext) error {
	err := binary.Write(to, binary.LittleEndian, uint64(len(o.Fields)))
	if err != nil {
		return err
	}
	for k, v := range o.Fields {
		err = binary.Write(to, binary.LittleEndian, uint64(k))
		if err != nil {
			return err
		}
		err = binary.Write(to, binary.LittleEndian, v.Tag())
		if err != nil {
			return err
		}
		err = v.serialize(to)
		if err != nil {
			return err
		}
	}
	return nil
}

type Integer struct {
	Value int64
}

func (Integer) isMsg() {}

var _ Msg = Integer{}

func (i Integer) Tag() byte {
	return 1
}

func (i Integer) Equals(other Msg) bool {
	v, ok := other.(Integer)
	if !ok {
		return false
	}
	return i.Value == v.Value
}

func (i Integer) serialize(to SerializationContext) error {
	err := binary.Write(to, binary.LittleEndian, uint64(i.Value))
	if err != nil {
		return err
	}
	return nil
}

type Float struct {
	Value float64
}

func (Float) isMsg() {}

var _ Msg = Float{}

func (f Float) Tag() byte {
	return 2
}

func (f Float) Equals(other Msg) bool {
	v, ok := other.(Float)
	if !ok {
		return false
	}
	return f.Value == v.Value
}

func (f Float) serialize(to SerializationContext) error {
	err := binary.Write(to, binary.LittleEndian, float64(f.Value))
	if err != nil {
		return err
	}
	return nil
}

type Data struct {
	Value []byte
}

func (Data) isMsg() {}

var _ Msg = Data{}

func (d Data) Tag() byte {
	return 3
}

func (d Data) Equals(other Msg) bool {
	v, ok := other.(Data)
	if !ok {
		return false
	}
	return bytes.Equal(v.Value, d.Value)
}

func (d Data) serialize(to SerializationContext) error {
	err := binary.Write(to, binary.LittleEndian, uint64(len(d.Value)))
	if err != nil {
		return err
	}
	n, err := to.Write(d.Value)
	if err != nil {
		return fmt.Errorf("failed to write all data, had %d bytes but only wrote %d: %w", len(d.Value), n, err)
	}
	return nil
}

type Handle struct {
	Descriptor uintptr
}

func (Handle) isMsg() {}

var _ Msg = Handle{}

func (h Handle) Tag() byte {
	return 4
}

func (h Handle) Equals(other Msg) bool {
	v, ok := other.(Handle)
	if !ok {
		return false
	}
	return v.Descriptor == h.Descriptor
}

func (h Handle) serialize(to SerializationContext) error {
	idx, ok := to.SendFileHandle(h.Descriptor)
	if !ok {
		return fmt.Errorf("could not send file descriptor for handle")
	}
	err := binary.Write(to, binary.LittleEndian, uint64(idx))
	if err != nil {
		return err
	}
	return nil
}

type Array[C Msg] struct {
	Elems []C
}

type IArray interface {
	Msg

	Add(msg Msg)
}

func (a Array[C]) Tag() byte {
	return 5
}

func (a *Array[C]) Equals(other Msg) bool {
	v, ok := other.(*Array[C])
	if !ok {
		return false
	}
	if len(v.Elems) != len(a.Elems) {
		return false
	}
	for idx := range a.Elems {
		if !a.Elems[idx].Equals(v.Elems[idx]) {
			return false
		}
	}
	return true
}

func (*Array[C]) isMsg() {}

func (a *Array[C]) serialize(to SerializationContext) error {
	var c C

	err := binary.Write(to, binary.LittleEndian, uint64(len(a.Elems)))
	if err != nil {
		return err
	}
	err = binary.Write(to, binary.LittleEndian, c.Tag())
	if err != nil {
		return err
	}
	for _, it := range a.Elems {
		err = it.serialize(to)
		if err != nil {
			return err
		}
	}
	return nil
}

func (a *Array[C]) Add(msg Msg) {
	v, ok := msg.(C)
	if !ok {
		panic("adding element of wrong type to array")
	}
	a.Elems = append(a.Elems, v)
}

func MakeArray(tag byte) (IArray, error) {
	switch tag {
	case TypeObject:
		return &Array[Object]{}, nil
	case TypeInteger:
		return &Array[Integer]{}, nil
	case TypeFloat:
		return &Array[Float]{}, nil
	case TypeData:
		return &Array[Data]{}, nil
	case TypeHandle:
		return &Array[Handle]{}, nil
	case TypeArray:
		return nil, fmt.Errorf("no nested arrays")
	default:
		return nil, fmt.Errorf("unrecognised tag %d", tag)
	}
}

var _ Msg = &Array[Integer]{}
var _ IArray = &Array[Integer]{}
