package binformat_test

import (
	"Permutatio/binformat"
	"bytes"
	"testing"

	"github.com/alecthomas/repr"
)

func TestBasic(t *testing.T) {
	obj := binformat.Object{
		Fields: map[uint64]binformat.Msg{
			1: binformat.Integer{Value: 10},
			2: binformat.Float{Value: 3.5},
			3: binformat.Data{Value: []byte("youre moms r gay <3")},
			4: &binformat.Array[binformat.Integer]{Elems: []binformat.Integer{{0}, {1}, {2}}},
		},
	}
	ser := binformat.DummySerializationContext{}
	err := binformat.Serialize(obj, &ser)
	if err != nil {
		t.Error(err)
	}
	de := binformat.DummyDeserializationContext{*bytes.NewReader(ser.Buffer.Bytes())}
	obj2, err := binformat.Deserialize(&de)
	if err != nil {
		t.Error(err)
	}
	if !obj.Equals(obj2) {
		repr.Println(obj)
		repr.Println(obj2)
		t.Error("objects aren't equal")
	}
}

func TestComplex(t *testing.T) {
	obj := binformat.Object{
		Fields: map[uint64]binformat.Msg{
			1: binformat.Integer{Value: 10},
			2: binformat.Float{Value: 3.5},
			3: binformat.Data{Value: []byte("youre moms r gay <3")},
			4: binformat.Object{
				Fields: map[uint64]binformat.Msg{
					1: binformat.Integer{Value: 10},
					2: binformat.Float{Value: 3.5},
					3: binformat.Data{Value: []byte("youre moms r gay <3")},
					5: binformat.Object{
						Fields: map[uint64]binformat.Msg{
							1: binformat.Integer{Value: 10},
							2: binformat.Float{Value: 3.5},
							3: binformat.Data{Value: []byte("youre moms r gay <3")},
						},
					},
					7: &binformat.Array[binformat.Integer]{
						Elems: []binformat.Integer{
							{1},
							{3},
							{5},
							{8},
						},
					},
				},
			},
		},
	}
	ser := binformat.DummySerializationContext{}
	err := binformat.Serialize(obj, &ser)
	if err != nil {
		t.Error(err)
	}
	de := binformat.DummyDeserializationContext{*bytes.NewReader(ser.Buffer.Bytes())}
	obj2, err := binformat.Deserialize(&de)
	if err != nil {
		t.Error(err)
	}
	if !obj.Equals(obj2) {
		t.Error("objects aren't equal")
	}
}
