package binformat

import (
	"bytes"
	"io"
)

type SerializationContext interface {
	io.Writer

	SendFileHandle(hdl uintptr) (index uint64, ok bool)
}

type DummySerializationContext struct {
	bytes.Buffer
}

func (d DummySerializationContext) SendFileHandle(hdl uintptr) (index uint64, ok bool) {
	return 0, false
}

var _ SerializationContext = &DummySerializationContext{}

type DeserializationContext interface {
	io.Reader

	RecvFileHandle(index uint64) (hdl uintptr, ok bool)
}

type DummyDeserializationContext struct {
	bytes.Reader
}

func (d DummyDeserializationContext) RecvFileHandle(index uint64) (hdl uintptr, ok bool) {
	return 0, false
}

var _ DeserializationContext = &DummyDeserializationContext{}
