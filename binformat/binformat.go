package binformat

import (
	"encoding/binary"
	"fmt"
)

const (
	TypeObject  = 0
	TypeInteger = 1
	TypeFloat   = 2
	TypeData    = 3
	TypeHandle  = 4
	TypeArray   = 5
)

func read[C any](from DeserializationContext) (C, error) {
	var val C
	err := binary.Read(from, binary.LittleEndian, &val)
	return val, err
}

func deserializeWithoutTag(tag byte, from DeserializationContext) (Msg, error) {
	switch tag {
	case TypeObject:
		obj := Object{map[uint64]Msg{}}

		numFields, err := read[uint64](from)
		if err != nil {
			return nil, fmt.Errorf("failed to read the number of fields: %w", err)
		}
		for i := uint64(0); i < numFields; i++ {
			fieldNum, err := read[uint64](from)
			if err != nil {
				return nil, fmt.Errorf("failed to read field number: %w", err)
			}
			inner, err := Deserialize(from)
			if err != nil {
				return nil, fmt.Errorf("failed to read inner field content: %w", err)
			}
			obj.Fields[fieldNum] = inner
		}

		return obj, nil
	case TypeInteger:
		val, err := read[int64](from)
		if err != nil {
			return nil, fmt.Errorf("failed to read int value: %w", err)
		}
		return Integer{Value: val}, nil
	case TypeFloat:
		val, err := read[float64](from)
		if err != nil {
			return nil, fmt.Errorf("failed to read float value: %w", err)
		}
		return Float{Value: val}, nil
	case TypeData:
		size, err := read[uint64](from)
		if err != nil {
			return nil, fmt.Errorf("failed to read data size: %w", err)
		}
		bytes := make([]byte, size)
		n, err := from.Read(bytes)
		if err != nil {
			return nil, fmt.Errorf("failed to read data: %w", err)
		}
		if n < len(bytes) {
			return nil, fmt.Errorf("less data was available when reading binary data than expected (%d, got %d)", size, n)
		}
		return Data{Value: bytes}, nil
	case TypeHandle:
		idx, err := read[uint64](from)
		if err != nil {
			return nil, fmt.Errorf("failed to read handle index: %w", err)
		}
		hdl, ok := from.RecvFileHandle(idx)
		if !ok {
			return nil, fmt.Errorf("failed to receive file descriptor %d from the stream", idx)
		}
		return Handle{Descriptor: hdl}, nil
	case TypeArray:
		len, err := read[uint64](from)
		if err != nil {
			return nil, fmt.Errorf("failed to read array length: %w", err)
		}
		kind, err := read[byte](from)
		if err != nil {
			return nil, fmt.Errorf("failed to read array type: %w", err)
		}
		a, err := MakeArray(kind)
		if err != nil {
			return nil, fmt.Errorf("failed to make array: %w", err)
		}
		for i := uint64(0); i < len; i++ {
			el, err := deserializeWithoutTag(kind, from)
			if err != nil {
				return nil, fmt.Errorf("failed to deserialize element %d in array: %w", i, err)
			}
			a.Add(el)
		}
		return a, nil
	default:
		return nil, fmt.Errorf("unrecognised tag: %d", tag)
	}
}

func Deserialize(from DeserializationContext) (Msg, error) {
	tag, err := read[byte](from)
	if err != nil {
		return nil, fmt.Errorf("failed to read tag: %w", err)
	}
	return deserializeWithoutTag(tag, from)
}
