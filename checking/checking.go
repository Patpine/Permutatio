package checking

import (
	"Permutatio/cst"
	"Permutatio/module"
	"Permutatio/utils"
	"fmt"
	"os"
	"path"
)

type Environment struct {
	modules map[string]*module.Module
}

func NewEnvironment() *Environment {
	return &Environment{modules: map[string]*module.Module{}}
}

type Context struct {
	Env   *Environment
	Cur   *module.Module
	names utils.Map[string, module.Object]
}

func (c *Context) findName(n string) utils.Option[module.Object] {
	if t := c.names.Get(n); t.HasValue() {
		return t
	}
	if t := module.BuiltinModule.Child(n); t.HasValue() {
		return t
	}
	return utils.None[module.Object]()
}

func (c *Context) Lookup(t cst.Type) (module.Type, error) {
	switch v := t.(type) {
	case cst.TypeReference:
		obj := c.findName(v.Names[0])
		if !obj.HasValue() {
			return nil, LocationError{v.Pos, fmt.Sprintf("%s not fonud", v.Names[0])}
		}
		for _, name := range v.Names[1:] {
			inner := obj.Value().Child(name)
			if !inner.HasValue() {
				return nil, LocationError{v.Pos, fmt.Sprintf("%s not found in %s", name, obj.Value().Name())}
			}
			obj = inner
		}
		t, ok := obj.Value().(module.Type)
		if !ok {
			return nil, LocationError{v.Pos, fmt.Sprintf("%s is not a type", obj.Value().Name())}
		}
		return t, nil
		// if t := c.Cur.Type(v.Name); t.HasValue() {
		// 	return t.Value(), nil
		// }
		// if t := module.BuiltinModule.Type(v.Name); t.HasValue() {
		// 	return t.Value(), nil
		// }
		// return nil, LocationError{v.Pos, fmt.Sprintf("%s not found", v.Name)}
	case cst.ArrayType:
		inner, err := c.Lookup(v.Element)
		if err != nil {
			return nil, err
		}
		if _, ok := inner.(*module.ArrayType); ok {
			return nil, LocationError{v.Pos, "Arrays cannot be nested"}
		}
		return &module.ArrayType{inner}, nil
	default:
		panic("unhandled")
	}
}

func (e *Environment) CheckSum(in cst.SumType, ctx *Context) (module.Type, error) {
	s := &module.SumType{}
	if ctx.names.Contains(in.Name) {
		return &module.ErrorType{ctx.Cur, "Invalid Type"}, LocationError{in.Pos, "Name already used"}
	}
	s.Parent = ctx.Cur
	s.Identifier = in.Name
	s.Error = in.Errorable
	ctx.names.Add(in.Name, s)
	for _, con := range in.Constructors {
		if ctx.names.Contains(con.Name) {
			return &module.ErrorType{ctx.Cur, in.Name}, LocationError{con.Pos, "Name already used"}
		}
		// ctx.names.Add(con.Name)
		var f []module.Field
		for _, fi := range con.Fields {
			t, err := ctx.Lookup(fi.Type)
			if err != nil {
				return &module.ErrorType{ctx.Cur, in.Name}, err
			}
			f = append(f, module.Field{fi.Name, t, uint(fi.FieldNumber)})
		}
		s.Constructors = append(s.Constructors, struct {
			Name   string
			Number uint
			Fields []module.Field
		}{
			Name:   con.Name,
			Number: uint(con.FieldNumber),
			Fields: f,
		})
	}
	ctx.Cur.Types = append(ctx.Cur.Types, s)
	return s, nil
}

func (e *Environment) CheckProduct(in cst.ProductType, ctx *Context) (module.Type, error) {
	p := &module.ProductType{}
	if ctx.names.Contains(in.Name) {
		return &module.ErrorType{ctx.Cur, "Invalid Type"}, LocationError{in.Pos, "Name already used"}
	}
	ctx.names.Add(in.Name, p)
	p.Parent = ctx.Cur
	p.Identifier = in.Name
	p.Error = in.Errorable
	var f []module.Field
	for _, fi := range in.Fields {
		t, err := ctx.Lookup(fi.Type)
		if err != nil {
			return &module.ErrorType{ctx.Cur, in.Name}, err
		}
		f = append(f, module.Field{fi.Name, t, uint(fi.FieldNumber)})
	}
	p.Fields = f
	return p, nil
}

func (e *Environment) CheckNotification(in cst.Notification, ctx *Context) (*module.Notification, error) {
	n := &module.Notification{}
	if ctx.names.Contains(in.Name) {
		panic("name already used")
	}
	ctx.names.Add(in.Name, n)
	n.Identifier = in.Name
	var f []module.Field
	for _, fi := range in.Arguments {
		t, e := ctx.Lookup(fi.Type)
		if e != nil {
			return nil, e
		}
		f = append(f, module.Field{fi.Name, t, uint(fi.FieldNumber)})
	}
	n.Arguments = f
	return n, nil
}

func (e *Environment) CheckRPC(in cst.RPC, ctx *Context) (*module.RPC, error) {
	r := &module.RPC{}
	if ctx.names.Contains(in.Name) {
		panic("name already used")
	}
	ctx.names.Add(in.Name, r)
	r.Identifier = in.Name
	var f []module.Field
	for _, fi := range in.Arguments {
		t, e := ctx.Lookup(fi.Type)
		if e != nil {
			return nil, e
		}
		f = append(f, module.Field{fi.Name, t, uint(fi.FieldNumber)})
	}
	r.Arguments = f
	r.ReturnValue = utils.None[module.Type]()
	if in.Returns != nil {
		t, e := ctx.Lookup(*in.Returns)
		if e != nil {
			return nil, e
		}
		r.ReturnValue = utils.Some(t)
	}
	r.ThrowValue = utils.None[module.Type]()
	if in.Throws != nil {
		t, e := ctx.Lookup(*in.Throws)
		if e != nil {
			return nil, e
		}
		if !t.Errorable() {
			return nil, LocationError{in.Pos, "The throws value must be an error type"}
		}
		r.ThrowValue = utils.Some(t)
	}
	return r, nil
}

func (e *Environment) LoadModule(named string, cwd string) (out *module.Module, err error) {
	if m, ok := e.modules[named]; ok {
		return m, nil
	}

	loc := path.Join(cwd, named) + ".p"
	fdata, err := os.ReadFile(loc)
	if err != nil {
		return nil, fmt.Errorf("module could not be read from disk: %w", err)
	}
	file, err := cst.Parser.ParseBytes(loc, fdata)
	if err != nil {
		return nil, fmt.Errorf("module could not be read from disk: %w", err)
	}
	mod, err := e.Check(*file, path.Dir(loc))
	if err != nil {
		return nil, fmt.Errorf("module failed to typecheck: %w", err)
	}

	e.modules[named] = &mod
	return e.modules[named], nil
}

func (e *Environment) Check(in cst.File, dir string) (out module.Module, err error) {
	ctx := Context{e, &out, utils.NewMap[string, module.Object]()}
	out.Identifier = in.Package
	cerr := error(nil)
	for _, decl := range in.Declarations {
		switch d := decl.(type) {
		case cst.SumType:
			p, err := e.CheckSum(d, &ctx)
			out.Types = append(out.Types, p)
			if err != nil {
				cerr = Concat(cerr, err)
			}
		case cst.ProductType:
			p, err := e.CheckProduct(d, &ctx)
			out.Types = append(out.Types, p)
			if err != nil {
				cerr = Concat(cerr, err)
			}
		case cst.RPC:
			p, err := e.CheckRPC(d, &ctx)
			out.RPCs = append(out.RPCs, p)
			if err != nil {
				cerr = Concat(cerr, err)
			}
		case cst.Notification:
			n, err := e.CheckNotification(d, &ctx)
			out.Notifications = append(out.Notifications, n)
			if err != nil {
				cerr = Concat(cerr, err)
			}
		case cst.Import:
			mod, err := e.LoadModule(path.Join(d.Path...), dir)
			if err != nil {
				cerr = Concat(cerr, err)
				break
			}
			if ctx.names.Contains(mod.Identifier) {
				cerr = Concat(cerr, LocationError{d.Pos, "name already used"})
				break
			}
			ctx.names.Add(mod.Identifier, mod)
			for _, expose := range d.Exposing {
				if ctx.names.Contains(expose.Name) {
					cerr = Concat(cerr, LocationError{d.Pos, "name already used"})
					continue
				}
				v := mod.Child(expose.Name)
				if !v.HasValue() {
					cerr = Concat(cerr, LocationError{d.Pos, fmt.Sprintf("%s not found in module %s", expose.Name, mod.Identifier)})
					continue
				}
				ctx.names.Add(expose.Name, v.Value())
			}
		default:
			panic("unhandled case")
		}
	}
	return out, cerr
}
