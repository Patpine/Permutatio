package checking

import (
	"fmt"
	"strings"

	"github.com/alecthomas/participle/v2/lexer"
)

func Concat(e1, e2 error) error {
	e := []error{}
	if v, ok := e1.(ConcatError); ok {
		e = append(e, v.Errors...)
	} else if e1 != nil {
		e = append(e, e1)
	}
	if v, ok := e2.(ConcatError); ok {
		e = append(e, v.Errors...)
	} else if e2 != nil {
		e = append(e, e2)
	}
	if len(e) == 0 {
		return nil
	}
	return ConcatError{e}
}

type ConcatError struct {
	Errors []error
}

func (c ConcatError) Error() string {
	var s []string
	for _, e := range c.Errors {
		s = append(s, e.Error())
	}
	return strings.Join(s, "\n")
}

type LocationError struct {
	Pos         lexer.Position
	Explanation string
}

func (l LocationError) Error() string {
	return fmt.Sprintf("%s:%d:%d - %s", l.Pos.Filename, l.Pos.Line, l.Pos.Column, l.Explanation)
}
